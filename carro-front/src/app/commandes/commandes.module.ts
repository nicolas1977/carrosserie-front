import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CommandesListComponent} from "../commandes-composants/commandes-list/commandes-list.component";



@NgModule({
  declarations: [
    CommandesListComponent,
  ],
  imports: [
    CommonModule
  ]
})
export class CommandesModule { }
