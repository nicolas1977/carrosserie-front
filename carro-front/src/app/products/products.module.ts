import { NgModule,NO_ERRORS_SCHEMA,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsListComponent} from "../composants-products-module/products.list/products.list.component";

/* ProductList not standalone composant rattached univoque this module */

@NgModule({
  declarations: [
    ProductsListComponent,

  ],
  imports: [
    CommonModule
  ],
  schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
      ]
})
export class ProductsModule { }
