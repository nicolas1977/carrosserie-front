import { Routes } from '@angular/router';
import {ProductsListComponent} from "./composants-products-module/products.list/products.list.component";
import {AppComponent} from "./app.component";
import {PageAccueilComponent} from "./accueil-composants/page-accueil/page-accueil.component";
import {CommandesListComponent} from "./commandes-composants/commandes-list/commandes-list.component";

export const routes: Routes = [

  { path: '', redirectTo: 'page-accueil', pathMatch:'full'},
  { path: 'app', component: AppComponent },
  { path: 'products-list', component: ProductsListComponent },
  { path: 'page-accueil', component: PageAccueilComponent },
  { path: 'commandes-list', component: CommandesListComponent }

];
