import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PageAccueilComponent} from "../accueil-composants/page-accueil/page-accueil.component";



@NgModule({
  declarations: [
    PageAccueilComponent
  ],
  imports: [
    CommonModule
  ]
})
export class AccueilModule {


}
